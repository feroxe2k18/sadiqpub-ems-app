import { NgModule } from '@angular/core';
import { ToBoolPipe } from './../pipes/to-bool/to-bool';
import { RatingPipe } from './../pipes/rating/rating';
@NgModule({
	declarations: [ToBoolPipe,
    RatingPipe],
	imports: [],
	exports: [ToBoolPipe,
    RatingPipe]
})
export class PipesModule {}
