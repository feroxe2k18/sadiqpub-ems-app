import { Pipe, PipeTransform } from '@angular/core';

/**
 * Generated class for the ToBoolPipe pipe.
 *
 * See https://angular.io/docs/ts/latest/guide/pipes.html for more info on
 * Angular Pipes.
 */
@Pipe({
  name: 'toBool',
})
export class ToBoolPipe implements PipeTransform {
  /**
   * Takes a value and makes it lowercase.
   */
  transform(value: string, ...args) {

    return !(['false', '0', ''].indexOf(String(value).toLowerCase().trim()) + 1);

  }
}
