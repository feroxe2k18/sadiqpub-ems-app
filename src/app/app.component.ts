import { Globals } from './globals';
import { Component, ViewChild } from '@angular/core';
import { Nav, Platform, NavController } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { Http } from "@angular/http";
import { ApiServicesProvider } from './../providers/api-services/api-services';
import { TabsPage } from '../pages/tabs-root/tabs';
import { HistoryPage } from './../pages/history/history';

/* Forms pages */
import { FeedbackFormSchoolPage } from '../pages/feedback-form-school/feedback-form-school';
import { OrderFormSchoolPage } from '../pages/order-form-school/order-form-school';
import { QuestionaireFormSchoolPage } from '../pages/questionaire-form-school/questionaire-form-school';

import { LoginPage } from "../pages/login-screen/login";

/*import { ChatHomePage } from './../pages/chat-home/chat-home';


import * as firebase from 'firebase';

const config = {
  apiKey: 'AIzaSyDADDtMPgM4VCf8Y3CpGxjVJZjWyX1ExRc',
  authDomain: 'chat-testing-ems.firebaseapp.com',
  databaseURL: 'https://chat-testing-ems.firebaseio.com/',
  projectId: 'chat-testing-ems',
  storageBucket: 'chat-testing-ems.appspot.com',
};*/



@Component({
  templateUrl: 'app.html',
  providers: [ApiServicesProvider]
})


export class MyApp {
  @ViewChild(Nav) nav: Nav;
  @ViewChild(NavController) navCtrl: NavController;

  rootPage: any = LoginPage;

  public user_info: any;

  pages: Array<{ title: string, component: any, icon: any }>;
  public user = {
    email: '',
    password: '',
  };



  constructor(
    public http: Http,
    public platform: Platform,
    public statusBar: StatusBar,
    public splashScreen: SplashScreen
  ) {
    this.initializeApp();

    // used for an example of ngFor and navigation
    this.pages = [

      { title: 'Home Page', component: TabsPage, icon: 'home' },
      { title: 'History', component: HistoryPage, icon: 'ios-archive' },
      { title: 'Sample Form', component: FeedbackFormSchoolPage, icon: 'ios-copy' },
      { title: 'Order Form', component: OrderFormSchoolPage, icon: 'ios-paper' },
      { title: 'Questionnaire From', component: QuestionaireFormSchoolPage, icon: 'ios-document' },
      //{ title: 'Chat', component: ChatHomePage, icon: 'ios-document'},
      { title: 'Logout', component: LoginPage, icon: 'ios-power' }
    ];

    this.user_info = Globals.user_info;
    this.hideSplashScreen();
    //firebase.initializeApp(config);

  }

  hideSplashScreen() {
    if (this.splashScreen) {
      setTimeout(() => {
        this.splashScreen.hide();
      }, 100);
    }
  }

  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  ionViewDidLoad() {
    this.user_info = Globals.user_info;
    console.log('app.component');
  }

  openPage(page) {

    if (page.component == LoginPage) {

      window.localStorage.removeItem('token');

    }
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.setRoot(page.component);
  }
}
