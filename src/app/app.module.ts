/* Import native Moduels */
import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { HttpModule } from "@angular/http";
import { IonicStorageModule } from '@ionic/storage';
import { BackgroundGeolocation } from '@ionic-native/background-geolocation';
import { Geolocation } from '@ionic-native/geolocation';
//import { AgmCoreModule } from '@agm/core';
import { AgmCoreModule } from '@agm/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";

/** Pages */
/* CheckIn/Out */
import { CheckinSchoolsListPage } from '../pages/checkin-schools-list/checkin-schools-list';
import { AddNewSchoolPage } from '../pages/add-new-school/add-new-school';
import { CheckinSchoolPage } from '../pages/checkin-school/checkin-school';
import { CheckoutSchoolPage } from '../pages/checkout-school/checkout-school';
import { SetLocationPage } from "../pages/set-location/set-location";

/* Custom Modules */
import { AutoCompleteModule } from 'ionic2-auto-complete';
import { CheckedinSchoolsService } from '../providers/complete-test-service/complete-test-service';


/* Import Main */
import { MyApp } from './app.component';
import { LoginPage } from "../pages/login-screen/login";
import { HomePage } from '../pages/home/home';
import { MapPage } from '../pages/all-check-ins/map';
import { Tab1Page } from '../pages/checked-in/tab1';
import { Tab3Page } from '../pages/notifications/tab3';
import { TabsPage } from '../pages/tabs-root/tabs';
import { TomorrowPlanPage } from './../pages/tomorrow-plan/tomorrow-plan';
import { TomorrowPlanAddPage } from './../pages/tomorrow-plan-add/tomorrow-plan-add';

/* Import Sub Pages */
/* Order pages */
import { OrderFormDetailPage } from '../pages/order-form-detail/order-form-detail'
import { OrderFormSchoolPage } from '../pages/order-form-school/order-form-school';
import { OrderFormPage } from '../pages/order-form/order-form';
import { OrderFormSpSubjectPage } from '../pages/order-form-subject/order-form-subject';
import { OrderFormMpSubjectPage } from '../pages/order-form-mp-subject/order-form-mp-subject';

/*History Pages */
import { HistoryPage } from '../pages/history/history';
import { QuestionnaireHistoryDetailPage } from '../pages/questionnaire-history-detail/questionnaire-history-detail';
import { SampleformHistoryDetailPage } from '../pages/sampleform-history-detail/sampleform-history-detail';
import { OrderformHistoryDetailPage } from '../pages/orderform-history-detail/orderform-history-detail';
import { HistoryCheckedinPage } from '../pages/history-checkedin/history-checkedin';
import { HistoryCheckinDetailPage } from './../pages/history-checkin-detail/history-checkin-detail';
import { HistoryQuestionnaireListPage } from '../pages/history-questionnaire-list/history-questionnaire-list';
import { HistorySampleListPage } from '../pages/history-sample-list/history-sample-list';
import { HistoryOrderListPage } from '../pages/history-order-list/history-order-list';
import { HistoryFilterPage } from '../pages/history-filter/history-filter';
import { HistoryOptionsPage } from './../pages/history-options/history-options';

/* Questionare pages */
import { FormPage } from '../pages/questionaire-form/form';
import { QuestionaireFormSchoolPage } from '../pages/questionaire-form-school/questionaire-form-school';

/*Sample/Feedback Pages */

import { SampleFormPubChoicePage } from '../pages/sample-form-pub-choice/sample-form-pub-choice';
import { SampleFormSpSubjectPage } from '../pages/sample-form-subject/sample-form-subject';
import { FeedBackPage } from '../pages/feedback-form/feed-back';
import { FeedbackFormSchoolPage } from '../pages/feedback-form-school/feedback-form-school';
import { FeedbackFormDetailPage } from '../pages/feedback-form-detail/feedback-form-detail';
import { EditFeedbackFormPage } from './../pages/edit-feedback-form/edit-feedback-form';


/* Modals */
import { CheckedinSchoolsModalPage } from '../pages/checkedin-schools-modal/checkedin-schools-modal';

/*Import Providers */
import { LocationTrackerProvider } from '../providers/location-tracker/location-tracker';
import { ApiServicesProvider } from '../providers/api-services/api-services';


// /* Pipe Modules */
// import { PipesModule } from './../pipes/pipes.module';
// import { ToBoolPipe } from '../pipes/to-bool/to-bool';

/*Firebase chat pages */
/*import { ChatHomePage } from '../pages/chat-home/chat-home';
import { SigninPage } from '../pages/signin/signin';
import { RoomPage } from '../pages/room/room';
import { AddRoomPage } from '../pages/add-room/add-room';*/

@NgModule({
  declarations: [
    MyApp,

    HomePage,
    // PipesModule,
    // ToBoolPipe,

    TabsPage,
    MapPage,
    Tab1Page,
    Tab3Page,
    TomorrowPlanPage,
    TomorrowPlanAddPage,
    CheckinSchoolsListPage,
    CheckoutSchoolPage,
    AddNewSchoolPage,
    CheckinSchoolPage,
    SetLocationPage,
    LoginPage,
    FormPage,
    FeedBackPage,
    FeedbackFormSchoolPage,
    OrderFormSchoolPage,
    QuestionaireFormSchoolPage,
    OrderFormPage,
    OrderFormDetailPage,
    FeedbackFormDetailPage,

    SampleFormPubChoicePage,
    SampleFormSpSubjectPage,
    EditFeedbackFormPage,
    OrderFormSpSubjectPage,
    OrderFormMpSubjectPage,

    HistoryPage,
    HistorySampleListPage,
    HistoryOrderListPage,
    HistoryFilterPage,
    HistoryCheckedinPage,
    HistoryCheckinDetailPage,
    HistoryQuestionnaireListPage,
    OrderformHistoryDetailPage,
    SampleformHistoryDetailPage,
    QuestionnaireHistoryDetailPage,
    HistoryOptionsPage,

    CheckedinSchoolsModalPage,
  ],
  imports: [
    HttpModule,
    BrowserModule,
    IonicStorageModule.forRoot(),
    IonicModule.forRoot(MyApp),
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyD6AH1f3Nv10DSjkRHDxiAYDg6jhId73OU',
      libraries: ["places"]
    }),
    AutoCompleteModule,
    BrowserModule,
    FormsModule,
    ReactiveFormsModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,

    HomePage,

    TabsPage,
    MapPage,
    Tab1Page,
    Tab3Page,
    TomorrowPlanPage,
    TomorrowPlanAddPage,
    CheckinSchoolsListPage,
    CheckoutSchoolPage,
    AddNewSchoolPage,
    CheckinSchoolPage,
    SetLocationPage,
    LoginPage,
    FormPage,
    FeedBackPage,
    FeedbackFormSchoolPage,
    OrderFormSchoolPage,
    QuestionaireFormSchoolPage,
    OrderFormPage,
    OrderFormDetailPage,
    FeedbackFormDetailPage,
    SampleFormPubChoicePage,

    EditFeedbackFormPage,
    SampleFormSpSubjectPage,
    OrderFormSpSubjectPage,
    OrderFormMpSubjectPage,

    HistoryPage,
    HistorySampleListPage,
    HistoryOrderListPage,
    HistoryFilterPage,
    HistoryCheckedinPage,
    HistoryCheckinDetailPage,
    HistoryQuestionnaireListPage,
    OrderformHistoryDetailPage,
    SampleformHistoryDetailPage,
    QuestionnaireHistoryDetailPage,
    HistoryOptionsPage,

    CheckedinSchoolsModalPage,


  ],
  providers: [
    HttpModule,
    BackgroundGeolocation,
    Geolocation,
    StatusBar,
    SplashScreen,
    LocationTrackerProvider,
    CheckedinSchoolsService,
    { provide: ErrorHandler, useClass: IonicErrorHandler },
    ApiServicesProvider,
  ]
})
export class AppModule { }
