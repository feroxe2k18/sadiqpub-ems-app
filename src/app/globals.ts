export var Globals = {

  //api_url : 'http://api.sadiqpublications.com/api',
  api_url : 'http://127.0.0.1:8000',
  user_info: [],
  results: [],
  subjectsClasses: [],
  getCheckinSchoolsList: [],
  checkedIn: false,
  msg: "Welcome to Wisdomsol",
  schoolsList: [],
  logMe : function(obj){
    console.log(obj);
  },
  location: {'lat':0,'lng':0},
  checked_in_school_name : '',
  checked_in_school_id : '',

  convertStringToNumber(value: string): number {
    return +value;
  }

};
