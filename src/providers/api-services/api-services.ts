import { Globals } from './../../app/globals';
import { Injectable } from '@angular/core';
import { Http , RequestOptions , Headers} from '@angular/http';
import 'rxjs/add/operator/map';
import {LoadingController, ToastController } from 'ionic-angular';
/*
  Generated class for the ApiServicesProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular DI.
*/
@Injectable()
export class ApiServicesProvider {

  baseUrl: String;

  constructor(
    private http:Http,
    private toastCtrl: ToastController,
    private loadingCtrl: LoadingController

  ){
      this.http = http;
      this.baseUrl = 'http://api.sadiqpublications.com/api';
  }


  public getCheckinSchoolsList(): any {

    let headers = new Headers();
    headers.set('Authorization', `Bearer ${localStorage.getItem('token')}`);
    let requestOptions = new RequestOptions({headers});
    return this.http.get(Globals.api_url+`/getCheckinSchoolsList`, requestOptions)
      .toPromise().then((response) => {
      Globals.getCheckinSchoolsList = response.json();
    });

  }


  public postLoginAuth(user_info:any){
    return this.http.post(this.baseUrl+'/login',user_info)
      .toPromise().then((response) => {
        localStorage.setItem('token',response.json().token);
        Globals.user_info = response.json().user_info;
        console.log(Globals.user_info);
    });
  }

}
