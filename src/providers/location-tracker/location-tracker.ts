import {
  Injectable,
  NgZone
} from '@angular/core';
import {
  BackgroundGeolocation
} from '@ionic-native/background-geolocation';
import {
  Geolocation,
  Geoposition
} from '@ionic-native/geolocation';
import {
  Http,
  Headers,
  RequestOptions
} from "@angular/http";
import 'rxjs/add/operator/filter';
import 'rxjs/add/operator/map';

/*
  Generated class for the LocationTrackerProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular DI.
*/
@Injectable()
export class LocationTrackerProvider {

  public watch: any;
  public lat: number = 0;
  public lng: number = 0;

  constructor(
    public zone: NgZone,
    private backgroundGeolocation: BackgroundGeolocation,
    private geolocation: Geolocation,
    public http: Http
  ) {
    console.log('Hello LocationTrackerProvider Provider');
  }

  public locateInfo = {
    LocationName: '',
    Lat: 0,
    Lng: 0,
    Position: ''
  };

  startTracking() {
    // Background Tracking
    let config = {
      desiredAccuracy: 0,
      stationaryRadius: 20,
      distanceFilter: 10,
      debug: true,
      interval: 30000
    };

    this.backgroundGeolocation.configure(config).subscribe((location) => {

      console.log('BackgroundGeolocation:  ' + location.latitude + ',' + location.longitude);

      console.log(location);

      // Run update inside of Angular's zone
      this.zone.run(() => {
        this.lat = location.latitude;
        this.lng = location.longitude;


        this.locateInfo.Lat = this.lat;
        this.locateInfo.Lng = this.lng;
        //this.locateInfo.Position = location;

        //this.checkIninfo.CheckOutDateTIme=new Date();

        let headers = new Headers();
        //console.log('${localStorage.getItem('token')}');

        headers.set('Authorization', `Bearer ${localStorage.getItem('token')}`);
        let requestOptions = new RequestOptions({
          headers
        });

        this.http.post('http://api.sadiqpublications.com/api/locate', this.locateInfo, requestOptions)
          .toPromise().then(
            response => {
              console.log(response);
            }

          );
      });

    }, (err) => {

      console.log(err);

    });

    // Turn ON the background-geolocation system.
    this.backgroundGeolocation.start();


    // Foreground Tracking

    let options = {
      frequency: 30000,
      enableHighAccuracy: true
    };

    this.watch = this.geolocation.watchPosition(options).filter((p: any) => p.code === undefined).subscribe((position: Geoposition) => {

      console.log(position);

      // Run update inside of Angular's zone
      this.zone.run(() => {
        this.lat = position.coords.latitude;
        this.lng = position.coords.longitude;
      });



      this.locateInfo.Lat = this.lat;
      this.locateInfo.Lng = this.lng;
      //this.locateInfo.Position = position;
      //this.checkIninfo.CheckOutDateTIme=new Date();

      let headers = new Headers();
      //console.log('${localStorage.getItem('token')}');

      headers.set('Authorization', `Bearer ${localStorage.getItem('token')}`);
      let requestOptions = new RequestOptions({
        headers
      });
      this.http.post('http://api.sadiqpublications.com/api/locate', this.locateInfo, requestOptions)
        .toPromise().then(
          response => {
            console.log(response);
          }

        );
    });

  }

  stopTracking() {

    console.log('stopTracking');

    this.backgroundGeolocation.finish();
    this.watch.unsubscribe();

  }

}
