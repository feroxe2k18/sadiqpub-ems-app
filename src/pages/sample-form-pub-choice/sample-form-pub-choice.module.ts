import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SampleFormPubChoicePage } from './sample-form-pub-choice';

@NgModule({
  declarations: [
    SampleFormPubChoicePage,
  ],
  imports: [
    IonicPageModule.forChild(SampleFormPubChoicePage),
  ],
})
export class SampleFormPubChoicePageModule {}
