import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { SampleFormSpSubjectPage } from '../sample-form-subject/sample-form-subject'


/**
 * Generated class for the SampleFormPubChoicePage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-sample-form-pub-choice',
  templateUrl: 'sample-form-pub-choice.html',
})
export class SampleFormPubChoicePage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SampleFormPubChoicePage');
  }

  sadiqPubSub(){
      this.navCtrl.push(SampleFormSpSubjectPage);
  }

  mumtazPubSub(){
    this.navCtrl.push(SampleFormSpSubjectPage);
  }

}
