import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CheckedinSchoolsModalPage } from './checkedin-schools-modal';

@NgModule({
  declarations: [
    CheckedinSchoolsModalPage,
  ],
  imports: [
    IonicPageModule.forChild(CheckedinSchoolsModalPage),
  ],
})
export class CheckedinSchoolsModalPageModule {}
