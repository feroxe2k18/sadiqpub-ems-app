import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ViewController } from 'ionic-angular';

/**
 * Generated class for the CheckedinSchoolsModalPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-checkedin-schools-modal',
  templateUrl: 'checkedin-schools-modal.html',
})
export class CheckedinSchoolsModalPage {

  data:any;
  auto_complete:any;
  selected_item:any;
  constructor(public viewCtrl:ViewController, public navCtrl: NavController, public navParams: NavParams) {

    this.data  = navParams.get('data');
    this.auto_complete = this.data;

    console.log(this.data);

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CheckedinSchoolsModalPage');
  }

  dismiss() {
    this.viewCtrl.dismiss();

  }

   getItems(ev) {
     // set val to the value of the ev target
     var val = ev.target.value;
     // if the value is an empty string don't filter the items
     if (val && val.trim() != '') {
       this.data = this.auto_complete.filter((item) => {
         return (item.toLowerCase().indexOf(val.toLowerCase()) > -1);
       })
     }
   }

   clearItems() {

    this.data = this.auto_complete;
    console.log('clear item fired');

   }
   selectedItem(item) {
     this.viewCtrl.dismiss(item);
     console.log('selected item'+item);
   }



}
