import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams , MenuController } from 'ionic-angular';
import { Http , RequestOptions , Headers} from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';
import { Globals } from "../../app/globals";


/**
 * Generated class for the Tab3Page page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-tab3',
  templateUrl: 'tab3.html',
})
export class Tab3Page {


  results : any;

  constructor(public navCtrl: NavController, public navParams: NavParams,public http: Http ,public menuCtrl: MenuController) {
  }

  ionViewDidLoad() {
    let headers = new Headers();
    headers.set('Authorization', `Bearer ${localStorage.getItem('token')}`);
    let requestOptions = new RequestOptions({headers});
    this.http.get(Globals.api_url+`/getToDoListByUserID`, requestOptions)
       .toPromise().then((response) => {

       this.results = response.json();
       console.log(this.results);

    });
  }


}
