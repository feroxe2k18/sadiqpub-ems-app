import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,ViewController } from 'ionic-angular';
import {
  FormGroup,
  FormControl

} from '@angular/forms';
/**
 * Generated class for the HistoryFilterPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-history-filter',
  templateUrl: 'history-filter.html',
})
export class HistoryFilterPage {

  filter;
  filterForm;
  custom_date   : String = new Date().toISOString();
  date_from = "";
  date_to = "";



  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public viewCtrl: ViewController
  ) {

    this.filter  = this.navParams.get('data');
    console.log(this.filter);

  }




  onSubmit() {
    this.viewCtrl.dismiss({"filter":this.filter,"date_from":this.date_from,"date_to":this.date_to});
  }
  onLeave() {
    this.viewCtrl.dismiss();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad HistoryFilterPage');
  }

}
