import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FeedBackPage } from './feed-back';
import { HttpModule } from '@angular/http';

@NgModule({
  declarations: [
    FeedBackPage,
  ],
  imports: [
    HttpModule,
    IonicPageModule.forChild(FeedBackPage),
  ],
})
export class FeedBackPageModule {


}