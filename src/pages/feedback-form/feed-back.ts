import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams ,MenuController } from 'ionic-angular';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';
import { Globals } from "../../app/globals";
import { SampleFormSpSubjectPage } from '../sample-form-subject/sample-form-subject'
import { FeedbackFormSchoolPage } from '../feedback-form-school/feedback-form-school'


@IonicPage()
@Component({
  selector: 'page-feed-back',
  templateUrl: 'feed-back.html',
})

export class FeedBackPage {
  posts: any;
  names: any;
  className: any;
  results : any;

  public SampleDetailParams ={
    school_id : '',
    subject_id : '',
    subject_name : '',
    school_name : '',
    p_index : 0
  };

  constructor(public navCtrl: NavController, public navParams: NavParams ,public menuCtrl: MenuController ,public http: Http) {
    this.SampleDetailParams.school_id = navParams.get('id');
    this.SampleDetailParams.school_name = navParams.get('school_name');
    this.menuCtrl = menuCtrl;
    this.menuCtrl.enable(false);
    this.results = Globals.subjectsClasses;

  }

  ionViewDidLoad() {
  }

  itemSelected(index){
    this.SampleDetailParams.p_index = index;
    this.navCtrl.push(SampleFormSpSubjectPage,this.SampleDetailParams);
  }

  back(){
    this.navCtrl.pop();
    this.navCtrl.setRoot(FeedbackFormSchoolPage);
  }

}
