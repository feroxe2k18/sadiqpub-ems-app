import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SampleFormSpSubjectPage } from './sample-form-subject';

@NgModule({
  declarations: [
    SampleFormSpSubjectPage,
  ],
  imports: [
    IonicPageModule.forChild(SampleFormSpSubjectPage),
  ],
})
export class SampleFormSubjectModule {}
