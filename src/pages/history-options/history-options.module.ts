import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { HistoryOptionsPage } from './history-options';

@NgModule({
  declarations: [
    HistoryOptionsPage,
  ],
  imports: [
    IonicPageModule.forChild(HistoryOptionsPage),
  ],
})
export class HistoryOptionsPageModule {}
