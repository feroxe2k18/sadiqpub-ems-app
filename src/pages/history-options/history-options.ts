import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Globals } from "../../app/globals";
import { HistoryCheckedinPage } from "../history-checkedin/history-checkedin";
import { HistorySampleListPage } from "../history-sample-list/history-sample-list";
import { HistoryOrderListPage } from "../history-order-list/history-order-list";
import { HistoryQuestionnaireListPage } from "../history-questionnaire-list/history-questionnaire-list";

/**
 * Generated class for the HistoryOptionsPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-history-options',
  templateUrl: 'history-options.html',
})
export class HistoryOptionsPage {

  my_results: any;
  classes: any;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.my_results = Globals.results[navParams.get('index')];
    console.log(this.my_results);
    //console.log("user id " +this.my_results.user_id);
   // console.log("school id " +this.my_results.school_id);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad HistoryOptionsPage');
  }

  show_checked_in_history(){
    this.navCtrl.push(HistoryCheckedinPage,{'my_results0' : this.my_results});
  }

  show_sample_form_order_list(){
    this.navCtrl.push(HistorySampleListPage,{'my_results0' : this.my_results});
  }

  show_order_form_list(){
    this.navCtrl.push(HistoryOrderListPage,{'my_results0' : this.my_results});
  }

  show_questionnaire_form_list(){
    this.navCtrl.push(HistoryQuestionnaireListPage,{'my_results0' : this.my_results});
  }

}
