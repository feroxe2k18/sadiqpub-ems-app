import { LoginPage } from './../login-screen/login';
import { Component  } from '@angular/core';
import {IonicPage, NavController, NavParams, Platform , MenuController,ModalController,ViewController} from 'ionic-angular';
import { LocationTrackerProvider } from '../../providers/location-tracker/location-tracker';
//import {GoogleMap, GoogleMapsEvent, LatLng } from '@ionic-native/google-maps';
import 'rxjs/add/operator/map';
import {Http , Headers, RequestOptions} from "@angular/http";
// import { AutoCompleteModule } from 'ionic2-auto-complete';
// import { CheckedinSchoolsService } from '../../providers/complete-test-service/complete-test-service';
import { CheckedinSchoolsModalPage } from '../../pages/checkedin-schools-modal/checkedin-schools-modal';

import 'rxjs/add/operator/toPromise';
import {FormPage} from "../questionaire-form/form";
import { FeedBackPage } from "../feedback-form/feed-back";
import { Globals } from "../../app/globals";




/**
 * Generated class for the Tab1Page page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-tab1',
  templateUrl: 'tab1.html',
})
export class Tab1Page {





  public checkIninfo = {
    LocationName : '',
    Lat: 0,
    Lng : 0,
    Purpose : '',
    token : ''
  };

  public user = {
    gotram:''
  }

  public isCheckin: boolean = false;

   public results  :  {
    checkedin:0;
  };


  //all-check-ins: GoogleMap;

  constructor(
              public modalCtrl: ModalController,
              public navCtrl: NavController,
              public navParams: NavParams ,
              public locationTracker: LocationTrackerProvider ,
              public platform: Platform ,
              public http: Http,
              public menuCtrl: MenuController,
              //public autoCompleteModule:AutoCompleteModule ,public checkedinSchools: CheckedinSchoolsService
            )
  {
    Globals.logMe(this);
    this.menuCtrl = menuCtrl;
    this.menuCtrl.enable(true);

    platform.ready().then(() =>
      {
        //this.loadMap();
      }
    )

  }

  public selectSchoolName() {
    let gotrams = ["Ali","Nabeel"];
    let data = { "title": "Gotram", "form": "show_autocomplete", "data": gotrams };
    let modal = this.modalCtrl.create(CheckedinSchoolsModalPage, data);
    modal.onDidDismiss(data => {
      if (data) {
        this.user.gotram = data;
      }
    });
    modal.present();

  }

  openModal() {
        let modal = this.modalCtrl.create( CheckedinSchoolsModalPage);
        modal.present();
  }


  ionViewDidLoad() {


    this.locationTracker.startTracking();

    let headers = new Headers();
    headers.set('Authorization', `Bearer ${localStorage.getItem('token')}`);
    let requestOptions = new RequestOptions({headers});

    this.http.get(Globals.api_url+`/ischeckin`, requestOptions)
        .toPromise().then((response) => {
          console.log(response);
          this.results = response.json();
          console.log(this.results);
    });
  }

  ionViewWillEnter() {

    let headers = new Headers();
    headers.set('Authorization', `Bearer ${localStorage.getItem('token')}`);
    let requestOptions = new RequestOptions({headers});

    this.http.get(Globals.api_url+`/ischeckin`, requestOptions)
        .toPromise().then((response) => {
          console.log(response);
          this.results = response.json();

          this.isCheckin = (this.results.checkedin)?true:false;
    });
    console.log("this function will be called every time you enter the view");
  }
  // Load all-check-ins only after view is initialized







  stop(){
    this.locationTracker.stopTracking();
  }

  public buttonClicked: boolean = false; //Whatever you want to initialise it as

  public onButtonClick() {

    this.buttonClicked = !this.buttonClicked;
  }

  /*ChecinSave() {
    this.http.post(globals.api_url+'/checkin',this.ChecIninfo)
      .toPromise().then((response) => {
      localStorage.getItem('token') = response.json().token;
      //this.navCtrl.setRoot(TabsPage)
    });
  }*/


  CheckInPost(){
    this.checkIninfo.Lat=this.locationTracker.lat;
    this.checkIninfo.Lng=this.locationTracker.lng;

    //this.checkIninfo.CheckOutDateTIme=new Date();

    let headers = new Headers();
    //console.log('${localStorage.getItem('token')}');
    console.log(this.checkIninfo);
    headers.set('Authorization', `Bearer ${localStorage.getItem('token')}`);
    let requestOptions = new RequestOptions({headers});
    this.http.post(Globals.api_url+'/checkin', this.checkIninfo, requestOptions)
      .toPromise().then(
        response => {
          console.log(response);
          this.isCheckin = true;
        }

      );
  }


  CheckOutPost(){
    this.checkIninfo.Lat=this.locationTracker.lat;
    this.checkIninfo.Lng=this.locationTracker.lng;
    //this.checkIninfo.CheckOutDateTIme=new Date();

    let headers = new Headers();
    //console.log('${localStorage.getItem('token')}');
    console.log(this.checkIninfo);
    headers.set('Authorization', `Bearer ${localStorage.getItem('token')}`);
    let requestOptions = new RequestOptions({headers});
    this.http.post(Globals.api_url+'/checkout', this.checkIninfo, requestOptions)
      .toPromise().then(
        response => {
          console.log(response);
          this.isCheckin = false;
        }

      );
  }

  logout(){
    this.navCtrl.setRoot(LoginPage);
  }

  Qform()
  {
    this.navCtrl.setRoot(FormPage);
  }

  Fform()
  {
    this.navCtrl.setRoot(FeedBackPage);
  }




}


export class ModalContentPage {
  character;

  constructor(
    public platform: Platform,
    public params: NavParams,
    public viewCtrl: ViewController
  ) {
    var characters = [
      {
        name: 'Gollum',
        quote: 'Sneaky little hobbitses!',
        image: 'assets/img/avatar-gollum.jpg',
        items: [
          { title: 'Race', note: 'Hobbit' },
          { title: 'Culture', note: 'River Folk' },
          { title: 'Alter Ego', note: 'Smeagol' }
        ]
      },
      {
        name: 'Frodo',
        quote: 'Go back, Sam! I\'m going to Mordor alone!',
        image: 'assets/img/avatar-frodo.jpg',
        items: [
          { title: 'Race', note: 'Hobbit' },
          { title: 'Culture', note: 'Shire Folk' },
          { title: 'Weapon', note: 'Sting' }
        ]
      },
      {
        name: 'Samwise Gamgee',
        quote: 'What we need is a few good taters.',
        image: 'assets/img/avatar-samwise.jpg',
        items: [
          { title: 'Race', note: 'Hobbit' },
          { title: 'Culture', note: 'Shire Folk' },
          { title: 'Nickname', note: 'Sam' }
        ]
      }
    ];
    this.character = characters[this.params.get('charNum')];
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }
}

