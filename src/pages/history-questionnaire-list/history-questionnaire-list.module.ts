import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { HistoryQuestionnaireListPage } from './history-questionnaire-list';

@NgModule({
  declarations: [
    HistoryQuestionnaireListPage,
  ],
  imports: [
    IonicPageModule.forChild(HistoryQuestionnaireListPage),
  ],
})
export class QuestionnaireFormListPageModule {}
