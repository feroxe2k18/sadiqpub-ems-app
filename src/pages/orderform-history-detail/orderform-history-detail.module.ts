import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { OrderformHistoryDetailPage } from './orderform-history-detail';

@NgModule({
  declarations: [
    OrderformHistoryDetailPage,
  ],
  imports: [
    IonicPageModule.forChild(OrderformHistoryDetailPage),
  ],
})
export class OrderformHistoryDetailPageModule {}
