import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, MenuController } from 'ionic-angular';
import { Http } from '@angular/http';
import { Globals } from "../../app/globals";

/**
 * Generated class for the OrderformHistoryDetailPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-orderform-history-detail',
  templateUrl: 'orderform-history-detail.html',
})

export class OrderformHistoryDetailPage {

  results: any;
  classes: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, public menuCtrl: MenuController, public http: Http) {
    this.results = Globals.results[navParams.get('index')];

    if (this.results.details != null) {
      console.log(this.results.details);
      for (let i in this.results.details) {
        console.log(this.results.details[i].class_name);
        this.results.details[i]['class_name'] = this.results.details[i].class_name.split(',');
        this.results.details[i]['qty_orderd'] = this.results.details[i].qty_orderd.split(',');
      }
    }
  }

}
