import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FeedbackFormSchoolPage } from './feedback-form-school';

@NgModule({
  declarations: [
    FeedbackFormSchoolPage,
  ],
  imports: [
    IonicPageModule.forChild(FeedbackFormSchoolPage),
  ],
})
export class FeedbackFormSchoolPageModule {}
