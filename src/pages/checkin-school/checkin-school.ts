import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { LoadingController, ToastController, ViewController } from "ionic-angular";
import { Component } from '@angular/core';
import { Http , Headers, RequestOptions } from "@angular/http";
import { Globals } from '../../app/globals';
import { Geolocation } from '@ionic-native/geolocation';

//import { PlacesService } from "../../services/places";

/**
 * Generated class for the CheckinSchoolPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-checkin-school',
  templateUrl: 'checkin-school.html',
})
export class CheckinSchoolPage {
  place: any;
  data: any;
  resp: any;
  tracking_id:0;
  other:'';
  onSamePlace: boolean = false;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private viewCtrl: ViewController,
    private loadingCtrl: LoadingController,
    private toastCtrl: ToastController,
    public http: Http,
    private geolocation: Geolocation,
  ) {
    this.place  = this.navParams.get('data');

    /*stroing ifo in globals.ts or cross app usage */
    /*Globals.checked_in_school_id=this.place.id;
    Globals.checked_in_school_name=this.place.school_name;*/

    /* use local storage to store information of checked in school id and name */

    console.log(this.place);

    console.log("School ID : " + this.place.id);
    console.log("School Name : " + this.place.school_name);

    /*saving info in the local storage*/
    localStorage.setItem("Checked-in-school-id",this.place.id);
    //console.log("from local storage" +(localStorage.getItem("Checked-in-school-id")));
    localStorage.setItem("Checked-in-school-name",this.place.school_name);

    this.geolocation.getCurrentPosition()
      .then(
      location => {
        //loader.dismiss();
        let c_lat = location.coords.latitude;
        let c_lng = location.coords.longitude;

        let distance = this.getDistanceFromLatLonInKm(c_lat,c_lng,this.place.lat,this.place.lng);
        distance = distance * 1000;
        console.log(distance);
        console.log(c_lat+','+c_lng);
        console.log(this.place.lat+','+this.place.lng);

        if(distance <= 30){
          this.onSamePlace = true;
        }

      }
      )
      .catch(
      error => {
        //loader.dismiss();
        const toast = this.toastCtrl.create({
          message: 'error in location!',
          duration: 2500
        });
        toast.present();
      }
      );

    //console.log(this.place);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CheckinSchoolPage');
    //console.log(this.place.school_name);
  }

  onLeave() {
    this.viewCtrl.dismiss();
    //this.navCtrl.setRoot(CheckinSchoolsListPage);
  }

  onSubmit() {

    const loader = this.loadingCtrl.create({
      content: 'Checking In Please Wait...'
    });

    loader.present();


    if(this.place.purpose == 'other'){this.place.purpose = 'other:'+this.other;}

    let headers = new Headers();
    headers.set('Authorization', `Bearer ${localStorage.getItem('token')}`);
    let requestOptions = new RequestOptions({headers});
    this.http.post(Globals.api_url+'/checkin', this.place, requestOptions)
      .toPromise().then(
        response => {
          loader.dismiss();

          this.resp = response.json();
          console.log(this.resp);

          const toast = this.toastCtrl.create({
            message: 'Checked In successfully',
            duration: 2500
          });
          toast.present();

          this.tracking_id = this.resp.tracking_id;
          console.log(this.tracking_id);



          Globals.checkedIn = true;
          localStorage.setItem('checkedinSchoolID',this.place.id);
          localStorage.setItem('isCheckedin','1');
          //localStorage.setItem("checked_in_school_id", this.place.id);
          localStorage.setItem("checked_in_schoolname", this.place.school_name);
          this.viewCtrl.dismiss({'tracking_id':this.tracking_id});

          //Globals.school_name=this.place.school_name;
        }

      ).catch(
        error => {
          loader.dismiss();
          const toast = this.toastCtrl.create({
            message: 'Error: Please try agian in few minutes.',
            duration: 2500
          });
          toast.present();
        }
      );

  }

  private convertStringToNumber(value: string): number {
    return +value;
  }



  private getDistanceFromLatLonInKm(lat1, lon1, lat2, lon2):number {
    var R = 6371; // Radius of the earth in km
    var dLat = this.deg2rad(lat2-lat1);  // deg2rad below
    var dLon = this.deg2rad(lon2-lon1);
    var a =
      Math.sin(dLat/2) * Math.sin(dLat/2) +
      Math.cos(this.deg2rad(lat1)) * Math.cos(this.deg2rad(lat2)) *
      Math.sin(dLon/2) * Math.sin(dLon/2)
      ;
    var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
    var d = R * c; // Distance in km
    return d;
  }

  private deg2rad(deg):number {
    return deg * (Math.PI/180)
  }


}
