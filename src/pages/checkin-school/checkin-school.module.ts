import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CheckinSchoolPage } from './checkin-school';

@NgModule({
  declarations: [
    CheckinSchoolPage,
  ],
  imports: [
    IonicPageModule.forChild(CheckinSchoolPage),
  ],
})
export class CheckinSchoolPageModule {}
