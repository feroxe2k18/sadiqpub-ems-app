import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { OrderFormMpSubjectPage } from './order-form-mp-subject';

@NgModule({
  declarations: [
    OrderFormMpSubjectPage,
  ],
  imports: [
    IonicPageModule.forChild(OrderFormMpSubjectPage),
  ],
})
export class OrderFormMpSubjectPageModule {}
