import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,MenuController } from 'ionic-angular';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';
import { Globals } from "../../app/globals";
import { OrderFormDetailPage } from "../order-form-detail/order-form-detail";
import { Http , RequestOptions , Headers} from '@angular/http';

/**
 * Generated class for the OrderFormMpSubjectPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-order-form-mp-subject',
  templateUrl: 'order-form-mp-subject.html',
})
export class OrderFormMpSubjectPage {

  posts: any;
  names: any;
  className: any;
  results : any;

  public SampleDetailParams ={
    school_id : '',
    subject_id : '',
    subject_name : '',
    school_name : ''
  };

  constructor(public navCtrl: NavController, public navParams: NavParams ,public http: Http ,public menuCtrl: MenuController) {
    this.SampleDetailParams.school_id=navParams.get('school_id');
    this.SampleDetailParams.school_name=navParams.get('school_name');

    this.menuCtrl = menuCtrl;
    this.menuCtrl.enable(false);

  }

  ionViewDidLoad() {
    let headers = new Headers();
    headers.set('Authorization', `Bearer ${localStorage.getItem('token')}`);
    let requestOptions = new RequestOptions({headers});
    this.http.get(Globals.api_url+`/getBooksClasses`, requestOptions)
      .toPromise().then((response) => {
      console.log('IN PROMISE');
      this.results = response.json();
      window.localStorage['getBooksClasses'] = response.toString()
      console.log(this.results);
    });
    console.log(this.results);
    console.log('call ends');
    console.log(this.SampleDetailParams.school_id);
    console.log(this.SampleDetailParams.school_name);
  }

  itemSelected(id,name){
    //console.log('in item selected');
    //console.log(this.SampleDetailParams);
    this.SampleDetailParams.subject_id = id;
    this.SampleDetailParams.subject_name = name;
    this.navCtrl.push(OrderFormDetailPage,this.SampleDetailParams);



  }

}
