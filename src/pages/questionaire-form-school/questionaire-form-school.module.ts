import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { QuestionaireFormSchoolPage } from './questionaire-form-school';

@NgModule({
  declarations: [
    QuestionaireFormSchoolPage,
  ],
  imports: [
    IonicPageModule.forChild(QuestionaireFormSchoolPage),
  ],
})
export class QuestionaireFormSchoolPageModule {}
