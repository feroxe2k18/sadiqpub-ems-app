import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,MenuController ,LoadingController } from 'ionic-angular';

import { Http , RequestOptions , Headers} from '@angular/http';
import { Globals } from "../../app/globals";
import { OrderFormDetailPage } from "../order-form-detail/order-form-detail"

@IonicPage()
@Component({
  selector: 'page-order-form-school',
  templateUrl: 'order-form-school.html',
})
export class OrderFormSchoolPage {

  results : any;
  auto_complete: any;
  checkedInSchoolID:any;

  school_id : any;
  school_name : any;

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public http: Http ,
    public menuCtrl: MenuController,
    private loadingCtrl: LoadingController    
  ) {
    this.getCheckinSchoolsList();

     /* getting info from the local storage*/
     this.school_id=localStorage.getItem('Checked-in-school-id');
     this.school_name=localStorage.getItem("Checked-in-school-name");
 
     console.log("School id : " + this.school_id);
     console.log("School name : " + this.school_name);

    this.menuCtrl = menuCtrl;
    this.menuCtrl.enable(true);
    if(localStorage.getItem('checkedinSchoolID')!=null){
      this.checkedInSchoolID = localStorage.getItem('checkedinSchoolID');
    }


  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad OrderFormSchoolPage');
  }

  itemSelected(id,school_name) {
    //this.navCtrl.push(OrderFormDetailPage,{'school_id':id,'school_name':school_name});

  }

  getCheckinSchoolsList(){
    let headers = new Headers();
    headers.set('Authorization', `Bearer ${localStorage.getItem('token')}`);
    let requestOptions = new RequestOptions({headers});
    this.http.get(Globals.api_url+`/getCheckinSchoolsList`, requestOptions)
      .toPromise().then((response) => {
      this.results = response.json();
      this.auto_complete = this.results;
    });

    this.http.get(Globals.api_url+`/getBooksClassesGroupByPublishers`, requestOptions)
      .toPromise().then((response) => {
      Globals.subjectsClasses = response.json();

    });

  }

  ionViewDidEnter(){
    this.presentLoadingDefault();    
  }

  initializeItems() {
    this.results = this.auto_complete ;
  }

  getItems(ev : any) {
    // Reset items back to all of the items
    this.initializeItems();

    // set val to the value of the ev target
    var val = ev.target.value;

    // if the value is an empty string don't filter the items
    if (val && val.trim() != '') {
      this.results = this.results.filter((item) => {
        return (item.school_name.toLowerCase().indexOf(val.toLowerCase()) > -1);
      })
    }
  }

  presentLoadingDefault() {
    let loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });
  
    loading.present();
  
    setTimeout(() => {
      
      loading.dismiss();
      this.navCtrl.setRoot(OrderFormDetailPage,{'school_id':this.school_id,'school_name':this.school_name});
    }, 4000);
  }




}
