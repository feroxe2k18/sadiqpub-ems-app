import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { OrderFormSchoolPage } from './order-form-school';

@NgModule({
  declarations: [
    OrderFormSchoolPage,
  ],
  imports: [
    IonicPageModule.forChild(OrderFormSchoolPage),
  ],
})
export class OrderFormSchoolPageModule {}
