import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CheckoutSchoolPage } from './checkout-school';

@NgModule({
  declarations: [
    CheckoutSchoolPage,
  ],
  imports: [
    IonicPageModule.forChild(CheckoutSchoolPage),
  ],
})
export class CheckoutSchoolPageModule {}
