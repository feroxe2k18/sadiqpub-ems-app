import { Globals } from './../../app/globals';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the CheckoutSchoolPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-checkout-school',
  templateUrl: 'checkout-school.html',
})
export class CheckoutSchoolPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CheckoutSchoolPage');
    console.log("Checked in school id " +Globals.checked_in_school_id);
  }


  CheckOutPost(){

  }
  cancel(){

  }
}
