import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams ,MenuController } from 'ionic-angular';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';
import { Globals } from "../../app/globals";
import { OrderFormDetailPage } from "../order-form-detail/order-form-detail"

/**
 * Generated class for the OrderFormSpSubjectPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-order-form-sp-subject',
  templateUrl: 'order-form-subject.html',
})
export class OrderFormSpSubjectPage {

  posts: any;
  names: any;
  className: any;
  results : any;
  items : any;


  public SampleDetailParams ={
    school_id : '',
    subject_id : '',
    subject_name : '',
    school_name : '',
    p_index: 0,
    b_index: 0,

  };

  constructor(public navCtrl: NavController, public navParams: NavParams,public http: Http ,public menuCtrl: MenuController) {
    this.SampleDetailParams.school_id = navParams.get('school_id');
    this.SampleDetailParams.school_name = navParams.get('school_name');
    this.SampleDetailParams.p_index = navParams.get('p_index');
    this.results = Globals.subjectsClasses[this.SampleDetailParams.p_index]['books'];
    this.initializeItems();

  }

  ionViewDidLoad() {
  }

  initializeItems() {
    this.items = this.results;
  }

  getItems(ev : any) {
    // Reset items back to all of the items
    this.initializeItems();

    // set val to the value of the ev target
    var val = ev.target.value;

    // if the value is an empty string don't filter the items
    if (val && val.trim() != '') {
      this.items = this.items.filter((items) => {
        return (items.book_name.toLowerCase().indexOf(val.toLowerCase()) > -1);
      })
    }
  }

  itemSelected(id,name,index){
    this.SampleDetailParams.subject_id = id;
    this.SampleDetailParams.subject_name = name;
    this.SampleDetailParams.b_index = index;
    this.navCtrl.push(OrderFormDetailPage,this.SampleDetailParams);
  }


}
