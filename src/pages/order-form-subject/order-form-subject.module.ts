import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { OrderFormSpSubjectPage } from './order-form-subject';

@NgModule({
  declarations: [
    OrderFormSpSubjectPage,
  ],
  imports: [
    IonicPageModule.forChild(OrderFormSpSubjectPage),
  ],
})
export class OrderFormSpSubjectPageModule {}
