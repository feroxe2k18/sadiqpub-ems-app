
import { Component } from '@angular/core';
import { IonicPage, NavParams,ViewController } from 'ionic-angular';
import { Location } from "../../models/location";


import { ElementRef, NgZone, OnInit, ViewChild } from '@angular/core';
import { FormControl } from "@angular/forms";

import { MapsAPILoader } from '@agm/core';
import {} from '@types/googlemaps';


declare var google: any;


/**
 * Generated class for the SetLocationPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */


@IonicPage()
@Component({
  selector: 'page-set-location',
  templateUrl: 'set-location.html',
})


export class SetLocationPage implements OnInit {

  public latitude: number;
  public longitude: number;
  public searchControl: FormControl;
  public zoom: number;


  @ViewChild("search")
  public searchElementRef: ElementRef;

  ngOnInit() {
    //set google maps defaults
    this.zoom = 4;
    this.latitude = 39.8282;
    this.longitude = -98.5795;

    //create search FormControl
    this.searchControl = new FormControl();

    //set current position
    this.setCurrentPosition();

    //load Places Autocomplete
    this.mapsAPILoader.load().then(() => {
      let autocomplete = new google.maps.places.Autocomplete(this.searchElementRef.nativeElement, {
        types: ["address"]
      });
      autocomplete.addListener("place_changed", () => {
        this.ngZone.run(() => {
          //get the place result
          let place: google.maps.places.PlaceResult = autocomplete.getPlace();

          //verify result
          if (place.geometry === undefined || place.geometry === null) {
            return;
          }

          //set latitude, longitude and zoom
          this.location.lat = place.geometry.location.lat();
          this.location.lng= place.geometry.location.lng();
          this.zoom = 14;
        });
      });
    });
  }

  private setCurrentPosition() {
    if ("geolocation" in navigator) {
      navigator.geolocation.getCurrentPosition((position) => {
        this.latitude = position.coords.latitude;
        this.longitude = position.coords.longitude;
        this.zoom = 14;
      });
    }
  }


  location: Location;
  marker: Location;

  constructor(private navParams: NavParams,
              private viewCtrl: ViewController,
              private mapsAPILoader: MapsAPILoader,
              private ngZone: NgZone
            ) {
    this.location = this.navParams.get('location');
    console.log(this.location);
    if (this.navParams.get('isSet')) {
      this.marker = this.location;
      console.log("Yes Set");
    }
  }

  onSetMarker(event: any) {
    console.log(event);
    this.marker = new Location(event.coords.lat, event.coords.lng);
  }

  onConfirm() {
    this.viewCtrl.dismiss({location: this.marker});
  }

  onAbort() {
    this.viewCtrl.dismiss();
  }

}
