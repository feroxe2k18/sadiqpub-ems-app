import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, MenuController, LoadingController, ToastController }
  from 'ionic-angular';

import { TabsPage } from "../tabs-root/tabs";
import 'rxjs/add/operator/map';
import { Http } from "@angular/http";
import 'rxjs/add/operator/toPromise';


import { Globals } from "../../app/globals";
/**
 * Generated class for the LoginPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  public user = {
    email: '',
    password: '',
  };

  constructor(

    public navCtrl: NavController,
    public navParams: NavParams,
    public http: Http,
    public menuCtrl: MenuController,
    private toastCtrl: ToastController,
    private loadingCtrl: LoadingController) {


  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
    if (localStorage.getItem('token') != undefined) {
      this.navCtrl.setRoot(TabsPage)
    }
    //this.splashScreen.show();
  }

  login() {
    const loader = this.loadingCtrl.create({
      content: 'Loging In...'
    });
    loader.present();

    this.http.post(Globals.api_url + '/login', this.user)
      .toPromise().then((response) => {
        loader.dismiss();
        localStorage.setItem('token', response.json().token);
        Globals.user_info = response.json().user_info;

        console.log(Globals.user_info);
        this.navCtrl.setRoot(TabsPage)
      })
      .catch(
      error => {
        loader.dismiss();
        const toast = this.toastCtrl.create({
          message: 'Wrong Usermane or Password, Please Try Again.',
          duration: 2500
        });
        toast.present();
      }
      );

  }

}
