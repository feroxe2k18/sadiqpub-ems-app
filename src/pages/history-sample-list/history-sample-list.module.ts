import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { HistorySampleListPage } from './history-sample-list';

@NgModule({
  declarations: [
    HistorySampleListPage,
  ],
  imports: [
    IonicPageModule.forChild(HistorySampleListPage),
  ],
})
export class SampleformOrderListPageModule {}
