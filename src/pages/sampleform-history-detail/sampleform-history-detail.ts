import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, MenuController } from 'ionic-angular';
import { ModalController, LoadingController } from 'ionic-angular';
import { Http, RequestOptions, Headers } from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';
import { Globals } from "../../app/globals";
import { HistoryPage } from './../history/history';
import { EditFeedbackFormPage } from './../edit-feedback-form/edit-feedback-form';

/**
 * Generated class for the SampleformHistoryDetailPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-sampleform-history-detail',
  templateUrl: 'sampleform-history-detail.html',
})

export class SampleformHistoryDetailPage {

  results: any;
  classes: any;
  rootPage: any = HistoryPage;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public modalCtrl: ModalController,
    public menuCtrl: MenuController,
    public http: Http
  ) {

    console.log(Globals.results);
    this.results = Globals.results[navParams.get('index')];
    console.log(this.results);


    if (this.results.details != null) {
      for (let i in this.results.details) {

        if(this.results.details[i].sample_picked instanceof Array){
        }else{
          this.results.details[i]['class_name'] = this.results.details[i].class_name.split(',');
          this.results.details[i]['sample_asked'] = this.results.details[i].sample_asked.split(',');
          this.results.details[i]['sample_droped'] = this.results.details[i].sample_droped.split(',');
          this.results.details[i]['sample_picked'] = this.results.details[i].sample_picked.split(',');
          this.results.details[i]['school_feedback_detail_id'] = this.results.details[i].school_feedback_detail_id.split(',');


        }
      }
    }
    console.log(this.results);
    console.log(Globals.results[navParams.get('index')]);

  }

  back() {
    this.navCtrl.pop();
    //this.navCtrl.setRoot(this.rootPage);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SampleformHistoryDetailPage');
    console.log(Globals.results);
  }

  editSampleForm() {

    let modal = this.modalCtrl.create(EditFeedbackFormPage, { 'data': this.results });
    modal.present();

    modal.onDidDismiss(data => {
      if (data) {
        console.log(data);

      }
    });

  }


}
