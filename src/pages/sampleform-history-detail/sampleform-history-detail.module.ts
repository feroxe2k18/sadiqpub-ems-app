import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SampleformHistoryDetailPage } from './sampleform-history-detail';

@NgModule({
  declarations: [
    SampleformHistoryDetailPage,
  ],
  imports: [
    IonicPageModule.forChild(SampleformHistoryDetailPage),
  ],
})
export class SampleformHistoryDetailPageModule {}
