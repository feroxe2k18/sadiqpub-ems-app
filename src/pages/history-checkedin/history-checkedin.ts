import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, MenuController, ModalController } from 'ionic-angular';
import { LoadingController, ToastController } from "ionic-angular";
import { Http, RequestOptions, Headers } from '@angular/http';
import { HistoryFilterPage } from './../history-filter/history-filter';
import { HistoryCheckinDetailPage } from './../history-checkin-detail/history-checkin-detail';
import { HistoryPage } from './../history/history';
import { Globals } from "../../app/globals";


/**
 * Generated class for the CheckedInHistoryPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-history-checkedin',
  templateUrl: 'history-checkedin.html',
})
export class HistoryCheckedinPage {

  rootPage: any = HistoryPage;
  results: any;
  filter = "This Week";
  date_from = "-";
  date_to = "-";
  my_results_2 :any;

  school_id : any;


  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public menuCtrl: MenuController,
    public http: Http,
    public modalCtrl: ModalController,
    private loadingCtrl: LoadingController,
    private toastCtrl: ToastController
  ) {
    this.my_results_2=this.navParams.get('my_results0');
    console.log(this.my_results_2);
    this.school_id=this.my_results_2.id;
    console.log("School Id : " +this.my_results_2.id);

    //console.log(this.results);
  }


  ionViewDidLoad() {
    this.getCheckinSchoolsList();
  }

  itemSelected(index) {
    this.navCtrl.push(HistoryCheckinDetailPage, { 'index': index ,'school_info':this.my_results_2 });
  }

  back() {
    this.navCtrl.pop();
   // this.navCtrl.setRoot(this.rootPage);
  }

  onFilter() {

    let modal = this.modalCtrl.create(HistoryFilterPage, { 'data': this.filter });
    modal.present();

    modal.onDidDismiss(data => {
      if (data) {
        console.log(data);
        this.filter = data.filter;
        this.date_from = (data.date_from != '') ? data.date_from : '-';
        this.date_to = (data.date_to != '') ? data.date_to : '-';
        this.getCheckinSchoolsList();
      }
    });

  }

  getCheckinSchoolsList() {

    const loader = this.loadingCtrl.create({
      content: 'Getting List...'
    });

    loader.present();
    let headers = new Headers();
    headers.set('Authorization', `Bearer ${localStorage.getItem('token')}`);
    let requestOptions = new RequestOptions({ headers });

    this.http.get(Globals.api_url + `/getCheckinsSchoolsList/` +this.school_id/* + this.filter + '/' + this.date_from + '/' + this.date_to*/, requestOptions)
      .toPromise().then((response) => {
        loader.dismiss();
        Globals.results = this.results = response.json();
        console.log(this.results);
      }).catch(
      error => {
        loader.dismiss();
        const toast = this.toastCtrl.create({
          message: 'Error: Please try agian in few minutes OR Report Admin.',
          duration: 5000
        });
        toast.present();
      }
      );
  }


}
