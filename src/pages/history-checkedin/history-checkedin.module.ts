import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { HistoryCheckedinPage } from './history-checkedin';

@NgModule({
  declarations: [
    HistoryCheckedinPage,
  ],
  imports: [
    IonicPageModule.forChild(HistoryCheckedinPage),
  ],
})
export class CheckedInHistoryPageModule {}
