import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { HistoryOrderListPage } from './history-order-list';

@NgModule({
  declarations: [
    HistoryOrderListPage,
  ],
  imports: [
    IonicPageModule.forChild(HistoryOrderListPage),
  ],
})
export class OrderFormListPageModule {}
