import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ModalController, LoadingController, ToastController } from "ionic-angular";
import { Component } from '@angular/core';
import { Geolocation } from '@ionic-native/geolocation';
import { Http, Headers, RequestOptions } from "@angular/http";
import { Location } from "../../models/location";
import { SetLocationPage } from "../set-location/set-location";
import { CheckinSchoolPage } from '../checkin-school/checkin-school';
import { CheckinSchoolsListPage } from '../checkin-schools-list/checkin-schools-list';
import { Globals } from '../../app/globals';

//import { PlacesService } from "../../services/places";

//import { AgmCoreModule } from '@agm/core';
/**
 * Generated class for the AddNewSchoolPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-add-new-school',
  templateUrl: 'add-new-school.html',
})
export class AddNewSchoolPage {

  location: Location = {
    lat: 40.7624324,
    lng: -73.9759827
  };
  locationIsSet = false;
  imageUrl = '';
  response: any;


  public schoolInfo = {
    school_id: 0,
    school_name: '',
    school_address: 'Placeholder',
    branch_name: 'Main Branch',
    lat: 0,
    lng: 0,
  };

  public data: any;
  public schoolList = [];
  public selectedSchoolName = "";
  changeStatus: any;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private modalCtrl: ModalController,
    private loadingCtrl: LoadingController,
    private toastCtrl: ToastController,
    private geolocation: Geolocation,
    public http: Http
  ) {

    this.changeStatus = this.navParams.get("change");

    if (Globals.location.lat == 0) {
      this.onLocate();
    } else {
      //location = Location { .lat };
      this.location.lat = Globals.location.lat;
      this.location.lng = Globals.location.lng;
      this.locationIsSet = true;
    }

    this.schoolList = Globals.schoolsList;



  }


  changeParentStatus() {
    this.changeStatus();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AddNewSchoolPage');
  }

  onLocate() {
    const loader = this.loadingCtrl.create({
      content: 'Getting your Location...'
    });
    loader.present();

    this.geolocation.getCurrentPosition()
      .then(
      location => {
        loader.dismiss();
        this.location.lat = location.coords.latitude;
        this.location.lng = location.coords.longitude;
        this.locationIsSet = true;
      }
      )
      .catch(
      error => {
        loader.dismiss();
        const toast = this.toastCtrl.create({
          message: 'Could get location, please pick it manually!',
          duration: 2500
        });
        toast.present();
      }
      );
  }


  onOpenMap() {
    const modal = this.modalCtrl.create(SetLocationPage,
      { location: this.location, isSet: this.locationIsSet });
    modal.present();
    modal.onDidDismiss(
      data => {
        if (data) {
          this.location = data.location;
          this.locationIsSet = true;
        }
      }
    );
  }

  initializeItems() {
    this.schoolList = Globals.schoolsList;
  }

  onSchoolKeyup(ev: any) {
    // Reset items back to all of the items
    this.initializeItems();

    // set val to the value of the ev target
    var val = ev.target.value;

    // if the value is an empty string don't filter the items
    if (val && val.trim() != '') {
      this.schoolList = this.schoolList.filter((item) => {
        return (
          item.school_name.toLowerCase().indexOf(val.toLowerCase()) > -1 ||
          item.school_address.toLowerCase().indexOf(val.toLowerCase()) > -1
        );
      })
    }
  }

  selectedSchool(i: number) {
    this.schoolInfo.school_name = this.schoolList[i].school_name;
    this.schoolInfo.school_address = this.schoolList[i].school_address;
    this.schoolInfo.branch_name = this.schoolList[i].branch_name;
    this.selectedSchoolName = this.schoolList[i].school_name;
    this.location.lat = this.convertStringToNumber(this.schoolList[i].lat);
    this.location.lng = this.convertStringToNumber(this.schoolList[i].lng);


  }
  onSubmit() {


    const loader = this.loadingCtrl.create({
      content: 'Saving Please Wait...'
    });
    loader.present();

    this.schoolInfo.lat = this.location.lat;
    this.schoolInfo.lng = this.location.lng;

    let headers = new Headers();

    console.log(this.schoolInfo);

    headers.set('Authorization', `Bearer ${localStorage.getItem('token')}`);
    let requestOptions = new RequestOptions({ headers });
    this.http.post(Globals.api_url + '/addNewSchool', this.schoolInfo, requestOptions)
      .toPromise().then(
      response => {
        loader.dismiss();

        this.response = response.json();

        console.log(this.response);
        //this.navCtrl.pop({'from':'add-new-school','data':this.response.complete_record});


        Globals.schoolsList.push(this.response.complete_record);
        //this.navCtrl.popTo(CheckinSchoolsListPage, { 'from': 'add-new-school', 'data': this.response.complete_record })
        this.navCtrl.pop();
        this.schoolInfo = this.response.complete_record;

        let modal2 = this.modalCtrl.create(CheckinSchoolPage, { 'data': this.schoolInfo });
        modal2.present();

        modal2.onDidDismiss(data => {
          if (data) {
            console.log(data);
            this.changeParentStatus();
          }

        });


      }

      ).catch(
      error => {
        loader.dismiss();
        const toast = this.toastCtrl.create({
          message: 'Error: Please try agian in few minutes.',
          duration: 2500
        });
        toast.present();
      }
      );
  }


  convertStringToNumber(value: string): number {
    return +value;
  }

}
