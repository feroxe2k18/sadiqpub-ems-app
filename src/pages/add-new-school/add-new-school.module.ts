import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AddNewSchoolPage } from './add-new-school';

@NgModule({
  declarations: [
    AddNewSchoolPage,
  ],
  imports: [
    IonicPageModule.forChild(AddNewSchoolPage),
  ],
})
export class AddNewSchoolPageModule {}
