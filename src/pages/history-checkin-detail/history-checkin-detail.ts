import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, MenuController } from 'ionic-angular';
import { Http } from '@angular/http';
import { Globals } from "../../app/globals";

/**
 * Generated class for the HistoryCheckinDetailPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-history-checkin-detail',
  templateUrl: 'history-checkin-detail.html',
})
export class HistoryCheckinDetailPage {

  results: any;
  classes: any;
  school_infor : any;

  constructor(public navCtrl: NavController, public navParams: NavParams, public menuCtrl: MenuController, public http: Http) {
    this.menuCtrl = menuCtrl;
    this.menuCtrl.enable(false);
    this.results = Globals.results[navParams.get('index')];

    this.school_infor=navParams.get('school_info');
    console.log(this.school_infor);
  }

}

