import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { HistoryCheckinDetailPage } from './history-checkin-detail';

@NgModule({
  declarations: [
    HistoryCheckinDetailPage,
  ],
  imports: [
    IonicPageModule.forChild(HistoryCheckinDetailPage),
  ],
})
export class HistoryCheckinDetailPageModule {}
