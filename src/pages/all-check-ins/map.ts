import { Component, ViewChild, ElementRef } from '@angular/core';
import { IonicPage, NavController, NavParams, Platform, MenuController, LoadingController } from 'ionic-angular';
import { Http, Headers, RequestOptions } from "@angular/http";
import { Geolocation } from '@ionic-native/geolocation';
import 'rxjs/add/operator/map';

import { Location } from "../../models/location";
import { Globals } from '../../app/globals';
//declare var google;

@IonicPage()
@Component({
  selector: 'page-map',
  templateUrl: 'map.html',
})
export class MapPage {

  @ViewChild('map') mapElement: ElementRef;
  map: any;
  mlocation: any;
  results: any;
  posts: any;
  latLng: any;
  mapOptions: any;
  markers: any;
  regionals: any;
  current_loc_pin: any;
  visited_loc_pin: any;
  tobe_visit_loc_pin: any;
  currentregional: any;
  pins: any;

  location: Location = {
    lat: 31.483673,
    lng: 74.1861198
  };
  locationIsSet = false;

  constructor(public navCtrl: NavController,
    public geolocation: Geolocation,
    public platform: Platform,
    public http: Http,
    private loadingCtrl: LoadingController,
    public menuCtrl: MenuController) {
    this.current_loc_pin = "assets/curren_location.png";
    this.visited_loc_pin = "assets/blue_pin.png";
    this.tobe_visit_loc_pin = "assets/blue-dot.png";

    this.menuCtrl = menuCtrl;
    this.menuCtrl.enable(true);

    this.onLocate();

    this.loadPins();

    //this.platform.ready().then(() => this.loadMap());
    //this.loadPins();


  }

  onLocate() {
    const loader = this.loadingCtrl.create({
      content: 'Please Wait.'
    });
    loader.present();

    this.geolocation.getCurrentPosition()
      .then(
      location => {
        loader.dismiss();
        this.location.lat = location.coords.latitude;
        this.location.lng = location.coords.longitude;
        this.locationIsSet = true;
        Globals.location.lat = location.coords.latitude;
        Globals.location.lng = location.coords.longitude;
      }
      )
      .catch(
      error => {
        loader.dismiss();
        // const toast = this.toastCtrl.create({
        //   message: 'Could get location, please pick it manually!',
        //   duration: 2500
        // });
        // toast.present();
      }
      );



  }

  loadPins() {
    let headers = new Headers();
    headers.set('Authorization', `Bearer ${localStorage.getItem('token')}`);
    let requestOptions = new RequestOptions({ headers });
    this.http.get(Globals.api_url + `/getCheckins`, requestOptions)
      .toPromise().then((response) => {

        this.pins = response.json();
        console.log(this.pins);

      });

  }

  private convertStringToNumber(value: string): number {
    return +value;
  }

}
