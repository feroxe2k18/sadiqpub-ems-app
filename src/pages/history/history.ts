import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, MenuController, ModalController } from 'ionic-angular';
import { LoadingController, ToastController ,Platform } from "ionic-angular";
//import { Page } from '@ionic/ionic';
//import {Page} from 'ionic-angular';
import { HistoryFilterPage } from './../history-filter/history-filter';
import { FeedBackPage } from "../feedback-form/feed-back";
import { Http , RequestOptions , Headers} from '@angular/http';
import { Globals } from "../../app/globals";
import { HistoryCheckedinPage } from "../history-checkedin/history-checkedin";
import { HistoryOptionsPage } from './../history-options/history-options';
/*import { HistorySampleListPage } from "../history-sample-list/history-sample-list";
import { HistoryOrderListPage } from "../history-order-list/history-order-list";
import { HistoryQuestionnaireListPage } from "../history-questionnaire-list/history-questionnaire-list";*/


@Component({
  selector: 'page-history',
  templateUrl: 'history.html'
})
export class HistoryPage {

  results: any;
  filter = "This Week";
  date_from = "-";
  date_to = "-";
  item =[''];

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public platform: Platform,
    public http: Http,
    public menuCtrl: MenuController,
    private loadingCtrl: LoadingController,
    private toastCtrl: ToastController,
    public modalCtrl: ModalController,
  ){

    console.log("in history main page");

    this.menuCtrl = menuCtrl;
    this.menuCtrl.enable(true);
  }

  ionViewDidLoad() {
    this.getCheckinSchoolsList();
    this.menuCtrl.enable(true);
  }

  ionViewDidEnter(){
    this.getCheckinSchoolsList();
    this.menuCtrl.enable(true);
  }

  itemSelected(index) {
    // console.log(index);
     this.navCtrl.push(HistoryOptionsPage, { 'index': index });
   }

   onFilter() {

    let modal = this.modalCtrl.create(HistoryFilterPage, { 'data': this.filter });
    modal.present();

    modal.onDidDismiss(data => {
      if (data) {
        console.log(data);
        this.filter = data.filter;
        this.date_from = (data.date_from != '') ? data.date_from : '-';
        this.date_to = (data.date_to != '') ? data.date_to : '-';
        this.getCheckinSchoolsList();
      }
    });

  }

  getCheckinSchoolsList() {

    const loader = this.loadingCtrl.create({
      content: 'Getting List...'
    });

    loader.present();
    let headers = new Headers();
    headers.set('Authorization', `Bearer ${localStorage.getItem('token')}`);
    let requestOptions = new RequestOptions({ headers });

    this.http.get(Globals.api_url + `/getVisitedSchoolsList/`, requestOptions)
      .toPromise().then((response) => {
        loader.dismiss();
        Globals.results = this.results = response.json();
        console.log(this.results);
      }).catch(
      error => {
        loader.dismiss();
        const toast = this.toastCtrl.create({
          message: 'Error: Please try agian in few minutes OR Report Admin.',
          duration: 5000
        });
        toast.present();
      }
      );
  }

  /* original functions
  show_checked_in_history(){
    this.navCtrl.push(HistoryCheckedinPage);
  }

  show_sample_form_order_list(){
    this.navCtrl.push(HistorySampleListPage);
  }

  show_order_form_list(){
    this.navCtrl.push(HistoryOrderListPage);
  }

  show_questionnaire_form_list(){
    this.navCtrl.push(HistoryQuestionnaireListPage);
  }
  End of original functions
  */


}

