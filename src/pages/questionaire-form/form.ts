import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,MenuController, Events } from 'ionic-angular';
import { LoadingController, ToastController } from "ionic-angular";
import {Http , Headers, RequestOptions} from "@angular/http";
import 'rxjs/add/operator/toPromise';
import { Globals } from "../../app/globals";

/**
 * Generated class for the FormPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-form',
  templateUrl: 'form.html',
})
export class FormPage {

  isCheckedin = '0';

  school_id = '';
  school_name= '';
  followup_date   : String = new Date().toISOString();

  public userQues = {
    school_id:'',
    school_strength: 0,
    school_session: 0,
    school_branches: 0,
    potential_customer: false,
    followup_date: '',

    contact:[
      {
        name:'',
        phone:'',
        email:'',
        designation:'Princple',
        checked:true,
      },
      {
        name:'',
        phone:'',
        email:'',
        designation:'',
        checked:false,
      },
      {
        name:'',
        phone:'',
        email:'',
        designation:'',
        checked:false,
      }
    ],

    subjects : [
      {
      subject_name : 'Social Studies',
      classes : '',
      discount : '',
      publisher: '',
      },
      {
        subject_name : 'Maths',
        classes : '',
        discount : '',
        publisher: '',
      },
      {
        subject_name : 'English',
        classes : '',
        discount : '',
        publisher: '',
      },
      {
        subject_name : 'English Grammar',
        classes : '',
        discount : '',
        publisher: '',
      },
      {
        subject_name : 'Urdu',
        classes : '',
        discount : '',
        publisher: '',
      },
      {
        subject_name : 'Urdu Grammar',
        classes : '',
        discount : '',
        publisher: '',
      },
      {
        subject_name : 'Computer',
        classes : '',
        discount : '',
        publisher: '',
      },
      {
        subject_name : 'Islamiyat',
        classes : '',
        discount : '',
        publisher: '',
      },
      {
        subject_name : 'Science',
        classes : '',
        discount : '',
        publisher: '',
      },
      {
        subject_name : 'Arts',
        classes : '',
        discount : '',
        publisher: '',
      },

    ]
  };


  constructor(
    public navCtrl: NavController,
    public navParams: NavParams ,
    public http: Http ,
    private loadingCtrl: LoadingController,
    private toastCtrl: ToastController,
    public menuCtrl: MenuController,
    public events: Events
    ) {

      this.school_id = navParams.get('school_id');
      this.school_name = navParams.get('school_name');
  
      //this.school_id = localStorage.getItem("checked_in_school_id");
      //this.school_name = localStorage.getItem("checked_in_schoolname");
  
      console.log("Checked in school id : " + this.school_id);
      console.log("Checked in school name : " + this.school_name);


    //this.navCtrl = navCtrl;
    this.menuCtrl = menuCtrl;
    this.menuCtrl.enable(true);
    this.userQues.school_id = navParams.get('school_id');
    this.school_name = navParams.get('school_name');
  }

  ionViewDidEnter() {
    this.followup_date = new Date().toISOString();
    //console.log(Globals.checkedIn);

    if (localStorage.getItem('isCheckedin') != null) {
      this.isCheckedin = localStorage.getItem('isCheckedin');
    }
    this.events.publish('updateScreen');
    console.log('ionViewWillEnter checkin-school-list');
    console.log("CheckedIn: " + this.isCheckedin);


  }

  onSubmit()
  {

    this.userQues.followup_date = this.followup_date.toString();

    const loader = this.loadingCtrl.create({
      content: 'Submitting Order Please Wait...'
    });
    loader.present();

    let headers = new Headers();
    headers.set('Authorization', `Bearer ${localStorage.getItem('token')}`);
    let requestOptions = new RequestOptions({headers});
    this.http.post(Globals.api_url+'/postQuestionareForm', this.userQues, requestOptions)
      .toPromise().then(
        response => {
          loader.dismiss();
          if(response.json().error == 0){
            const toast = this.toastCtrl.create({
              message: 'Questionaire Saved Successfully',
              duration: 5000
            });
            toast.present();
            this.navCtrl.pop();
          }
        }
      ).catch(
        error => {
          loader.dismiss();
          const toast = this.toastCtrl.create({
            message: 'Error: Please try agian in few minutes.',
            duration: 3000
          });
          toast.present();
          return false;
        }
      );

  }


}
