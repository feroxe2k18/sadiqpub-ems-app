import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TomorrowPlanPage } from './tomorrow-plan';

@NgModule({
  declarations: [
    TomorrowPlanPage,
  ],
  imports: [
    IonicPageModule.forChild(TomorrowPlanPage),
  ],
})
export class TomorrowPlanPageModule {}
