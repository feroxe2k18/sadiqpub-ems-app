import { Component, NgZone } from '@angular/core';
import { IonicPage, NavController, NavParams,MenuController  }from 'ionic-angular';
import { ModalController, LoadingController, Events } from 'ionic-angular';
import { Http , RequestOptions , Headers} from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';
import { Globals } from "../../app/globals";

import { TomorrowPlanAddPage } from './../tomorrow-plan-add/tomorrow-plan-add';


/**
 * Generated class for the TomorrowPlanPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-tomorrow-plan',
  templateUrl: 'tomorrow-plan.html',
})
export class TomorrowPlanPage {

  results : any;

  constructor(public navCtrl: NavController,
    public modalCtrl: ModalController,
    public navParams: NavParams,
    public http: Http,
    private loadingCtrl: LoadingController,
    public events: Events,
    private zone: NgZone
  ) {
    this.events.subscribe('updateScreen', () => {
      this.zone.run(() => {
        console.log('force update the screen');
      });
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TomorrowPlanPage');

    //this.getTomorrowPlanByUserID();

  }

  ionViewDidEnter() {

    this.events.publish('updateScreen');
    this.getTomorrowPlanByUserID();

  }

  getTomorrowPlanByUserID(){

    let headers = new Headers();
    headers.set('Authorization', `Bearer ${localStorage.getItem('token')}`);
    let requestOptions = new RequestOptions({ headers });
    this.http.get(Globals.api_url + `/getTomorrowPlanByUserID`, requestOptions)
      .toPromise().then((response) => {
        this.results = response.json();
        console.log(this.results);
      });
  }

  addNewPlan(){
    this.navCtrl.push(TomorrowPlanAddPage, { change: this.ionViewDidEnter.bind(this) });
  }


}
