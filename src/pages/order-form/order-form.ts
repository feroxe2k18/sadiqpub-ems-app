import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams , MenuController } from 'ionic-angular';
import { Http , RequestOptions , Headers} from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';
import { Globals } from "../../app/globals";
import { OrderFormDetailPage } from '../order-form-detail/order-form-detail'




@IonicPage()
@Component({
  selector: 'page-order-form',
  templateUrl: 'order-form.html',
})
export class OrderFormPage {

  public orderDetailParams ={
    school_id : '',
    subject_id : '',
    subject_name : '',
    school_name : ''
  };

  public userOrder = {
    school_id : '',
    school_name: '',
    subjects : [
      {
        subject_name : 'social studies',
        subject_quantity : '',
        classes : [
          {
            class_id:1,
            class_name: 'Nursery',
            class_quantity: '',
            selected : false
          },
          {
            class_id:2,
            class_name: 'Prep',
            class_quantity: '',
            selected : false
          }

        ]
      },
      {
        subject_name : 'Maths',
        subject_quantity : '',
        classes : [
          {
            class_id:1,
            class_name: 'Nursery',
            class_quantity:'',
            selected : false
          },
          {
            class_id:2,
            class_name: 'Prep',
            class_quantity: '',
            selected : false
          }

        ]
      }
    ]
  };


  classes : {};
  results : any;
  selected_book: string;
  names: any;
  class_quantity : any;
  total_quantity : any;
  posts: any;
  className: any;
  sbm: any;

  constructor(public navCtrl: NavController,
            public navParams: NavParams,
            public http: Http ,
            public menuCtrl: MenuController) {

  this.orderDetailParams.school_id=navParams.get('id');
  this.orderDetailParams.school_name=navParams.get('school_name');

    this.menuCtrl = menuCtrl;
    this.menuCtrl.enable(true);
  }

  ionViewDidLoad() {
   let headers = new Headers();
    headers.set('Authorization', `Bearer ${localStorage.getItem('token')}`);
    let requestOptions = new RequestOptions({headers});
    this.http.get(Globals.api_url+`/getBooksClasses`, requestOptions)
      .toPromise().then((response) => {
      console.log('IN PROMISE');
      this.results = response.json();
      window.localStorage['getBooksClasses'] = response.toString()
      console.log(this.results);
    });

    console.log(this.results);
    console.log('call ends');
    console.log(this.orderDetailParams.school_id);
    console.log(this.orderDetailParams.school_name);
  }

 // public buttonClicked: boolean = true;

  /*public onChange(book) {
    //alert(book);
    this.selected_book = book;
  }*/

  getBookData(){
    //return this.http.get(globals.api_url+'/getBooksClasses').map(res => res.json());
  }

  orderFormSubmit()
  {
    //console.log(localStorage.getItem('token'));
    console.log(this.userOrder.subjects[0].classes[0].selected);
    console.log(this.total_quantity);
    //console.log(this.classes);
    /*console.log(globals.api_url+'/postQuestionareForm');
    let headers = new Headers();
    headers.set('Authorization', `Bearer ${localStorage.getItem('token')}`);
    let requestOptions = new RequestOptions({headers});
    this.http.post(globals.api_url+'/postQuestionareForm', this.userQues, requestOptions)
      .toPromise().then(
      response => {
        console.log(response);
      }

    );*/
  }


  itemSelected(id,name){
    console.log('in item selected');
    console.log(this.orderDetailParams);
    this.orderDetailParams.subject_id = id;
    this.orderDetailParams.subject_name = name;
    this.navCtrl.push(OrderFormDetailPage,this.orderDetailParams);

  }
}
