import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { OrderFormDetailPage } from './order-form-detail';

@NgModule({
  declarations: [
    OrderFormDetailPage,
  ],
  imports: [
    IonicPageModule.forChild(OrderFormDetailPage),
  ],
})
export class OrderFormDetailPageModule {}
