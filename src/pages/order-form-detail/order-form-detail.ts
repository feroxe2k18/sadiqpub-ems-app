import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, MenuController , Events} from 'ionic-angular';
import { LoadingController, ToastController } from "ionic-angular";
import { Globals } from "../../app/globals";
import 'rxjs/add/operator/toPromise';
import { Http , RequestOptions , Headers} from '@angular/http';

/**
 * Generated class for the OrderFormDetailPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-order-form-detail',
  templateUrl: 'order-form-detail.html',
})
export class OrderFormDetailPage {

  results : any;
  school_id : any;
  school_name : any;

  isCheckedin = '0';

  comments : any;
  selected : false;
  total_quantity : any;
  order_date : String = new Date().toISOString();
  required_date : String = new Date().toISOString();
  followup_date   : String = new Date().toISOString();

  classes: any;
  subjectsClasses : any;
  postdata = { master:[], detail:[]};



  constructor(
    public navCtrl: NavController,
    public navParams: NavParams ,
    public http: Http ,
    private loadingCtrl: LoadingController,
    private toastCtrl: ToastController,
    public menuCtrl: MenuController,
    public events: Events
    ) {
    this.school_id = navParams.get('school_id');
    this.school_name = navParams.get('school_name');

    console.log("Checked in school id : " + this.school_id);
    console.log("Checked in school name : " + this.school_name);

    this.subjectsClasses = Globals.subjectsClasses;
    this.menuCtrl = menuCtrl;
    this.menuCtrl.enable(true);
  }

  ionViewDidEnter() {

    if (localStorage.getItem('isCheckedin') != null) {
      this.isCheckedin = localStorage.getItem('isCheckedin');
    }
    this.events.publish('updateScreen');
    console.log('ionViewWillEnter checkin-school-list');
    console.log("CheckedIn: " + this.isCheckedin);

    this.order_date  = new Date().toISOString();
    this.required_date = new Date().toISOString();
    this.followup_date = new Date().toISOString();
  }

  onSubmit(){

    const loader = this.loadingCtrl.create({
      content: 'Submitting Order Please Wait...'
    });
    loader.present();



    this.postdata.master = [{
      "school_id" : this.school_id,
      "comments" : this.comments,
      "order_date" : this.order_date,
      "required_date" : this.required_date,
      "followup_date" : this.followup_date
    }];

    this.postdata.detail = this.subjectsClasses;

    console.log(this.postdata);
    let headers = new Headers();
    headers.set('Authorization', `Bearer ${localStorage.getItem('token')}`);

    let requestOptions = new RequestOptions({headers});
    this.http.post(Globals.api_url+'/postSchoolOrderForm', this.postdata, requestOptions)
      .toPromise().then(
      response => {

        loader.dismiss();
        if(response.json().error == 0){
          const toast = this.toastCtrl.create({
            message: 'Order Placed Successfully',
            duration: 5000
          });
          toast.present();


          this.navCtrl.pop();


        }

      }
    ).catch(
      error => {
        loader.dismiss();
        const toast = this.toastCtrl.create({
          message: 'Error: Please try agian in few minutes.',
          duration: 300
        });
        toast.present();
        return false;
      }
    );
  }




  // showAlert() {
  //   let alert = this.alertCtrl.create({
  //     title: 'Alert',
  //     subTitle: 'Submitted Successfully',
  //     buttons: [{text : 'Ok', handler : () => {this.navCtrl.pop()}}]
  //   });
  //   alert.present();

  // }
}
