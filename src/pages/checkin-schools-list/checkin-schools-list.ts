import { Component, NgZone } from '@angular/core';
import { IonicPage, NavController, NavParams, Item } from 'ionic-angular';
import { ModalController, LoadingController, Events } from 'ionic-angular';
import { Http, RequestOptions, Headers } from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';
import { Globals } from "../../app/globals";

import { Place } from "../../models/place";

import { AddNewSchoolPage } from '../add-new-school/add-new-school'
import { CheckinSchoolPage } from '../checkin-school/checkin-school';


/**
 * Generated class for the CheckinSchoolsListPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-checkin-schools-list',
  templateUrl: 'checkin-schools-list.html',
})
export class CheckinSchoolsListPage {



  results: any;
  auto_complete: any;
  places: Place[] = [];
  addnewschool = AddNewSchoolPage;
  isCheckedin = '0';
  tracking_id = 0;


  constructor(public navCtrl: NavController,
    public modalCtrl: ModalController,
    public navParams: NavParams,
    public http: Http,
    private loadingCtrl: LoadingController,
    public events: Events,
    private zone: NgZone
  ) {
    this.events.subscribe('updateScreen', () => {
      this.zone.run(() => {
        console.log('force update the screen');
      });
    });


  }

  ionViewDidEnter() {


    if (localStorage.getItem('isCheckedin') != null) {
      this.isCheckedin = localStorage.getItem('isCheckedin');
    }
    this.events.publish('updateScreen');
    console.log('ionViewWillEnter checkin-school-list');
    console.log("CheckedIn: " + this.isCheckedin);
  //  console.log("CheckedIn School: " + this.results.school_name);

  }

  ionViewDidLoad() {

    let headers = new Headers();
    headers.set('Authorization', `Bearer ${localStorage.getItem('token')}`);
    let requestOptions = new RequestOptions({ headers });
    this.http.get(Globals.api_url + `/getSchoolsList`, requestOptions)
      .toPromise().then((response) => {
        this.results = response.json();
        Globals.schoolsList = this.results;
        console.log(this.results);
      });

    if (localStorage.getItem('isCheckedin') != null) {
      this.isCheckedin = localStorage.getItem('isCheckedin');
    }
    //console.log(this.results.id);
    this.events.publish('updateScreen');

  }


  initializeItems() {
    this.results = Globals.schoolsList;
    console.log(Globals.schoolsList);
    this.events.publish('updateScreen');
  }

  getItems(ev: any) {
    // Reset items back to all of the items
    this.initializeItems();

    // set val to the value of the ev target
    var val = ev.target.value;

    // if the value is an empty string don't filter the items
    if (val && val.trim() != '') {
      this.results = this.results.filter((item) => {
        return (
          item.school_name.toLowerCase().indexOf(val.toLowerCase()) > -1 ||
          item.school_address.toLowerCase().indexOf(val.toLowerCase()) > -1
        );
      })
    }

    console.log(this.results);
    console.log("School Name : " + this.results.school_name);
  }


  addNewSchool() {
    this.navCtrl.push(AddNewSchoolPage, { change: this.ionViewDidEnter.bind(this) });

  }

  onOpenPlace(index: number) {
    //const modal = this.modalCtrl.create(CheckinSchoolPage, {place: place, index: index});
    console.log(this.results[index]);
    let modal = this.modalCtrl.create(CheckinSchoolPage, { 'data': this.results[index], checkedin: this.isCheckedin });
    modal.present();

    modal.onDidDismiss(data => {
      if (data) {
        console.log(data);
        this.tracking_id = data.tracking_id;
        this.isCheckedin = localStorage.getItem('isCheckedin');
      }
    });


    // modal.onDidDismiss(
    //   () => {
    //     this.places = this.placesService.loadPlaces();
    //   }
    // );

    // modal.onDidDismiss(data => {
    //   if (data) {
    //     this.places = data;
    //   }
    // });


  }





  CheckOutPost() {

    const loader = this.loadingCtrl.create({
      content: 'Checkout...'
    });
    loader.present();


    let headers = new Headers();

    headers.set('Authorization', `Bearer ${localStorage.getItem('token')}`);
    let requestOptions = new RequestOptions({ headers });
    this.http.post(Globals.api_url + '/checkout', { 'tracking_id': window.localStorage['tracking_id'] }, requestOptions)
      .toPromise().then(
      response => {
        loader.dismiss();
        localStorage.removeItem('Checked-in-school-id');
        localStorage.removeItem('Checked-in-school-name');
        console.log(localStorage.getItem("Checked-in-school-id"));
        console.log(localStorage.getItem("Checked-in-school-name"));
        localStorage.setItem('isCheckedin', '0');
        this.isCheckedin = localStorage.getItem('isCheckedin');
        this.events.publish('updateScreen');
      }

      );
  }


}
