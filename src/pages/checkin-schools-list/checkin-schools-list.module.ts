import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CheckinSchoolsListPage } from './checkin-schools-list';

@NgModule({
  declarations: [
    CheckinSchoolsListPage,
  ],
  imports: [
    IonicPageModule.forChild(CheckinSchoolsListPage),
  ],
})
export class CheckinSchoolsListPageModule {}
