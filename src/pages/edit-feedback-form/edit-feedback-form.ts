
import { Component, Pipe } from '@angular/core';
import { IonicPage, NavController, NavParams, MenuController,ViewController } from 'ionic-angular';
import { ModalController, LoadingController,ToastController } from 'ionic-angular';

import { Http, RequestOptions, Headers } from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';
import { Globals } from "../../app/globals";
import { DatePipe } from '@angular/common';

// import { ToBoolPipe } from './../../pipes/to-bool/to-bool';
// import { PipesModule } from './../../pipes/pipes.module';

/**
 * Generated class for the EditFeedbackFormPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-edit-feedback-form',
  templateUrl: 'edit-feedback-form.html',
})
export class EditFeedbackFormPage {
  index: any;

  items : any;
  sample_val : any;
  purpose : any;
  classes : any;

  results : any;
  id : any;
  school_id : any;
  school_name : any;
  comments : any;
  selected : false;
  total_quantity : any;
  followup_date   : String = new Date().toISOString();

  max_date : any;
  branch_name : any;
  feedback_date : any;
  school_address : any;
  status : any;


  subjectsClasses : any;
  postdata = { master:[], detail:[]};


  paramsArray = {
    school_id :'',
    school_name :'',
  };


  constructor(
    public navCtrl: NavController,
    public navParams: NavParams ,
    public http: Http ,
    private loadingCtrl: LoadingController,
    private toastCtrl: ToastController,
    private viewCtrl: ViewController,
    ) {
     this.results = navParams.get('data');

     console.log(this.results);
     this.id = this.results.id;
     this.school_id = this.results.school_id;
     this.school_name = this.results.school_name;

     this.branch_name = this.results.branch_name;
     this.feedback_date = this.results.feedback_date;
     this.school_address = this.results.school_address;
     this.status = this.results.status;


     this.followup_date = new Date(this.results.followup_date).toISOString();

     this.comments = this.results.comments;

     this.results.details.forEach((item, index) => {
      for (let i in item.class_name) {
            item.sample_asked[i] = this.toBoolean(item.sample_asked[i]);
            item.sample_droped[i] = this.toBoolean(item.sample_droped[i]);
            item.sample_picked[i] = this.toBoolean(item.sample_picked[i]);
          }
     });

  }




  onLeave(){
    this.viewCtrl.dismiss();
  }

  onSubmit(){


    this.results.comments = this.comments;
    this.results.followup_date = this.followup_date;
    this.results.status = this.status;

    console.log(this.results);
    const loader = this.loadingCtrl.create({
      content: 'Submitting Sample Request...'
    });
    loader.present();

    let headers = new Headers();
    headers.set('Authorization', `Bearer ${localStorage.getItem('token')}`);

    let requestOptions = new RequestOptions({headers});
    this.http.post(Globals.api_url+'/editSchoolFeedbacksForm', this.results, requestOptions)
      .toPromise().then(
        response => {
          loader.dismiss();
          if(response.json().error == 0){
            const toast = this.toastCtrl.create({
              message: 'Sample Updated Successfully',
              duration: 5000
            });
            toast.present();
            //this.navCtrl.pop();
            this.viewCtrl.dismiss();
          }
        }
      ).catch(
        error => {
          loader.dismiss();
          const toast = this.toastCtrl.create({
            message: 'Error: Please try agian in few minutes.',
            duration: 300
          });
          toast.present();
          return false;
        }
      );

  }

  toBoolean(bool){
    if (bool == '1' || bool == 1) return true;
    if (bool == '0' || bool == 0) return false;

  }


}
