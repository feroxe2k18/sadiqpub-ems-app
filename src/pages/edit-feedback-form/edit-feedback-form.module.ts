import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EditFeedbackFormPage } from './edit-feedback-form';
import { PipesModule } from './../../pipes/pipes.module';


@NgModule({
  declarations: [
    EditFeedbackFormPage,
  ],
  imports: [
    IonicPageModule.forChild(EditFeedbackFormPage),
    PipesModule,
  ],
})
export class EditFeedbackFormPageModule {}
