import { Component ,NgZone} from '@angular/core';
import { IonicPage, NavController, NavParams , MenuController , AlertController , Events} from 'ionic-angular';
import { LoadingController, ToastController } from "ionic-angular";
import { Globals } from "../../app/globals";
import 'rxjs/add/operator/toPromise';
import { Http , RequestOptions , Headers} from '@angular/http';

/**
 * Generated class for the FeedbackFormDetailPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-feedback-form-detail',
  templateUrl: 'feedback-form-detail.html',
})
export class FeedbackFormDetailPage {


  items : any;
  sample_val : any;
  purpose : any;
  classes : any;

  isCheckedin = '0'

  results : any;
  school_id : any;
  school_name : any;
  comments : any;
  selected : false;
  total_quantity : any;
  followup_date   : String = new Date().toISOString();

  subjectsClasses : any;
  postdata = { master:[], detail:[]};


  paramsArray = {
    school_id :'',
    school_name :'',
  };

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams ,
    public http: Http ,
    private loadingCtrl: LoadingController,
    private toastCtrl: ToastController,
    public menuCtrl: MenuController,
    public alertCtrl: AlertController,
    public events: Events,
    private zone: NgZone
    ) {
    //this.school_id = navParams.get('school_id');
    this.school_id = navParams.get('school_id');
    this.school_name = navParams.get('school_name');

    //this.school_id = localStorage.getItem("checked_in_school_id");
    //this.school_name = localStorage.getItem("checked_in_schoolname");

    console.log("Checked in school id : " + this.school_id);
    console.log("Checked in school name : " + this.school_name);

    this.subjectsClasses = Globals.subjectsClasses;
    this.menuCtrl = menuCtrl;
    this.menuCtrl.enable(true);
    //this.userCheck();

    this.events.subscribe('updateScreen', () => {
      this.zone.run(() => {
        console.log('force update the screen');
      });
    });
  }


  ionViewDidEnter() {
    this.followup_date = new Date().toISOString();
    //console.log(Globals.checkedIn);

    if (localStorage.getItem('isCheckedin') != null) {
      this.isCheckedin = localStorage.getItem('isCheckedin');
    }
    this.events.publish('updateScreen');
    console.log('ionViewWillEnter checkin-school-list');
    console.log("CheckedIn: " + this.isCheckedin);


  }

  userCheck()
  {
    /*if (localStorage.getItem('isCheckedin') != null) {
      this.isCheckedin = localStorage.getItem('isCheckedin');
     // console.log(this.isCheckedin);
     this.presentAlert();
    }
    else
    {
      this.isCheckedin = localStorage.getItem('isCheckedin');
      console.log("you are checked in");
    }*/
    if (Globals.checkedIn != null)
    {
     // this.presentAlert();
    }
    else if (Globals.checkedIn == null)
    {
      console.log("already checked in");
      this.presentcheckinAlert();
    }
  }

  presentAlert() {
    let alert = this.alertCtrl.create({
      title: 'Low battery',
      subTitle: '10% of battery remaining',
      buttons: ['Dismiss']
    });
    alert.present();
  }

  presentcheckinAlert() {
    let alert = this.alertCtrl.create({
      title: 'checked in ',
      subTitle: '10% of battery remaining',
      buttons: ['Dismiss']
    });
    alert.present();
  }



  onSubmit(){
    const loader = this.loadingCtrl.create({
      content: 'Submitting Sample Request...'
    });
    loader.present();

    this.postdata.master = [{
      "school_id" : this.school_id,
      "comments" : this.comments,
      "followup_date" : this.followup_date
    }];

    this.postdata.detail = this.subjectsClasses;

    console.log(this.postdata);

    let headers = new Headers();
    headers.set('Authorization', `Bearer ${localStorage.getItem('token')}`);

    let requestOptions = new RequestOptions({headers});
    this.http.post(Globals.api_url+'/postSchoolFeedbacksForm', this.postdata, requestOptions)
      .toPromise().then(
        response => {
          loader.dismiss();
          if(response.json().error == 0){
            const toast = this.toastCtrl.create({
              message: 'Sample Request Placed Successfully',
              duration: 5000
            });
            toast.present();
            this.navCtrl.pop();
          }
        }
      ).catch(
        error => {
          loader.dismiss();
          const toast = this.toastCtrl.create({
            message: 'Error: Please try agian in few minutes.',
            duration: 300
          });
          toast.present();
          return false;
        }
      );

  }

  chkAllBookClasses(publisher_index, book_index,sample_value){
    let check_all_asked = this.subjectsClasses[publisher_index]['books'][book_index]['check_all_asked']
    let check_all_droped = this.subjectsClasses[publisher_index]['books'][book_index]['check_all_droped']
    let check_all_picked = this.subjectsClasses[publisher_index]['books'][book_index]['check_all_picked']
    let checked = [];

    if(check_all_asked==true){checked = ['sa'];}
    if(check_all_asked==true && check_all_droped==true){checked = ['sa','sd'];}
    if(check_all_asked==true && check_all_droped==true && check_all_picked==true){checked = ['sa','sd','sp'];}

    //if(check_all_asked==false || check_all_asked==undefined){check_all_asked = }

    for (let i in this.subjectsClasses[publisher_index]['books'][book_index]['classes']) {
      this.subjectsClasses[publisher_index]['books'][book_index]['classes'][i]['checked'] = checked;
    }

    //console.log(this.subjectsClasses[publisher_index]['books'][book_index]);

  }

  getCheckinSchoolsList(){
    let headers = new Headers();
    headers.set('Authorization', `Bearer ${localStorage.getItem('token')}`);
    let requestOptions = new RequestOptions({headers});

    this.http.get(Globals.api_url+`/getCheckinSchoolsList`, requestOptions)
      .toPromise().then((response) => {
      this.results = response.json();

    });

    this.http.get(Globals.api_url+`/getBooksClassesGroupByPublishers`, requestOptions)
      .toPromise().then((response) => {
      this.subjectsClasses = response.json();
      //this.initializeItems();
    });
  }

}
