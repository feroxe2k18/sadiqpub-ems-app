import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FeedbackFormDetailPage } from './feedback-form-detail';

@NgModule({
  declarations: [
    FeedbackFormDetailPage,
  ],
  imports: [
    IonicPageModule.forChild(FeedbackFormDetailPage),
  ],
})
export class FeedbackFormDetailPageModule {}
