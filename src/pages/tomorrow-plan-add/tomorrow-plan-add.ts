import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ModalController, LoadingController, ToastController } from "ionic-angular";
import { Component } from '@angular/core';
import { Http, Headers, RequestOptions } from "@angular/http";
import { Globals } from '../../app/globals';

/**
 * Generated class for the TomorrowPlanAddPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-tomorrow-plan-add',
  templateUrl: 'tomorrow-plan-add.html',
})
export class TomorrowPlanAddPage {

  response: any;

  public postData = {
    location: '',
    description: '',
    school_name: '',
    task_status: 'Pending',
    todo_date: 0
  };


  location: any;
  description: any;
  school_name: any;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private modalCtrl: ModalController,
    private loadingCtrl: LoadingController,
    private toastCtrl: ToastController,
    public http: Http
  ) {

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TomorrowPlanAddPage');
  }

  onSubmit() {


    const loader = this.loadingCtrl.create({
      content: 'Saving Please Wait...'
    });

    loader.present();

    let headers = new Headers();

    console.log(this.postData);

    headers.set('Authorization', `Bearer ${localStorage.getItem('token')}`);
    let requestOptions = new RequestOptions({ headers });
    this.http.post(Globals.api_url + '/postTomorrowPlan', this.postData, requestOptions)
      .toPromise().then(
      response => {
        loader.dismiss();

        this.response = response.json();
        console.log(this.response);
        this.navCtrl.pop();

      }

      ).catch(
      error => {
        loader.dismiss();
        const toast = this.toastCtrl.create({
          message: 'Error: Please try agian in few minutes.',
          duration: 2500
        });
        toast.present();
      }
      );
  }

}
