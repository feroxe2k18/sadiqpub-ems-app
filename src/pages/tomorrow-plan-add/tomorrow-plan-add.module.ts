import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TomorrowPlanAddPage } from './tomorrow-plan-add';

@NgModule({
  declarations: [
    TomorrowPlanAddPage,
  ],
  imports: [
    IonicPageModule.forChild(TomorrowPlanAddPage),
  ],
})
export class TomorrowPlanAddPageModule {}
