import { Component } from '@angular/core'
import { CheckinSchoolsListPage } from '../checkin-schools-list/checkin-schools-list';
import { Tab1Page } from '../checked-in/tab1';
import { MapPage } from '../all-check-ins/map';
import { Tab3Page } from '../notifications/tab3';
import { TomorrowPlanPage } from './../tomorrow-plan/tomorrow-plan';

@Component({
  templateUrl: 'tabs.html'
})

export class TabsPage {
  tab1Root = MapPage;
  tab2Root = CheckinSchoolsListPage;
  tab3Root = Tab3Page;
  TomorrowPlan = TomorrowPlanPage;


  constructor() {

  }
}/**
 * Created by feroz on 09-Aug-17.
 */
