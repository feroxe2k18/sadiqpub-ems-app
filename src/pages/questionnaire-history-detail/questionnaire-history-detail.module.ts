import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { QuestionnaireHistoryDetailPage } from './questionnaire-history-detail';

@NgModule({
  declarations: [
    QuestionnaireHistoryDetailPage,
  ],
  imports: [
    IonicPageModule.forChild(QuestionnaireHistoryDetailPage),
  ],
})
export class QuestionnaireHistoryDetailPageModule {}
