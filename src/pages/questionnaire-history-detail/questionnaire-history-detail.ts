import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,MenuController} from 'ionic-angular';
import { Http } from '@angular/http';
import { Globals } from "../../app/globals";
/**
 * Generated class for the QuestionnaireHistoryDetailPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-questionnaire-history-detail',
  templateUrl: 'questionnaire-history-detail.html',
})
export class QuestionnaireHistoryDetailPage {

  results : any;
  subject_name : any;
  class_name : any;
  discount : any;

  constructor(public navCtrl: NavController, public navParams: NavParams ,public menuCtrl: MenuController ,public http: Http) {
    this.results = Globals.results[navParams.get('index')];
    // this.subject_name = this.results.subject_name.split(',');
    // this.class_name = this.results.class_name.split(',');
    // this.discount = this.results.discount.split(',');
  }

  back(){
    this.navCtrl.pop();
    //this.navCtrl.setRoot(this.rootPage);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SampleformHistoryDetailPage');
  }


}
